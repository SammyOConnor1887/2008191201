<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Address extends Model
{
    protected $table = 'addresses';

    protected $fillable = [
        'name',
        'city',
        'area',
        'street',
        'house',
        'add_info',
        'user_id'
    ];

    public $timestamps = true;
}
