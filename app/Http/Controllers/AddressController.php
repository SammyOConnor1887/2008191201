<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Address;

class AddressController extends Controller
{
    public function index()
    {
        $addresses = $this->showAll();
        return view('welcome', compact('addresses'));
    }

    public function showAll()
    {
        return Address::orderBy('name')->get();
    }

    public function create(Request $request)
    {
        $request['user_id'] = 1;
        $address = Address::create($request->all());
        return $address;
    }

    public function destroy($id) {
        $address = Address::find($id);
        $address->delete();
        return $address;
    }
}
