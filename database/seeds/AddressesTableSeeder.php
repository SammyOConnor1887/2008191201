<?php

use Illuminate\Database\Seeder;

class AddressesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('addresses')->insert([
            [
                'name' => 'home address',
                'city' => 'Dubai',
                'area' => 'Business Bay Area',
                'street' => 'Sheikh Zayed Road',
                'house' => 'Single',
                'add_info' => null,
                'user_id' => 1
            ],
            [
                'name' => 'work address',
                'city' => 'Dubai',
                'area' => 'Business Bay Area',
                'street' => 'Sheikh Zayed Road',
                'house' => 'Single Business Tower',
                'add_info' => 'Suite 2204',
                'user_id' => 1
            ]
        ]);
    }
}
