<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'Helena Afrassiabi',
            'email' => Str::random(5).'@gmail.com',
            'password' => bcrypt('password'),
        ]);
    }
}
