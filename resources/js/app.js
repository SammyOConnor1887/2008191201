require('./bootstrap');

$(document).ready(function() {
    // Request function
    function sendRequest(ajaxUrl, ajaxType, callback = false, ajaxData = null) {
        $.ajax({
            url: ajaxUrl,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            cache: false,
            data: ajaxData,
            type: ajaxType,
            dataType: "json",
            success: function (data) {
                if (callback) {
                    callback(data);
                }
            }
        });
    }

    // Request callbacks
    function displayAddress(data) {
        let addressData = {
            name: data.name,
            city: data.city + ', ',
            area: data.area,
            street: data.street ? ', ' + data.street : '',
            house: data.house ? ', ' + data.house : '',
            add_info: data.add_info ? ', ' + data.add_info : ''
        };

        let addressHtml = $([
            "<div class='item' data-address-id='" + data.id + "'>",
            "   <h3>" + addressData.name + "</h3>",
            "   <p>" + addressData.city + addressData.area + addressData.street + addressData.house + addressData.add_info + "</p>",
            "   <div class='actbox'>",
            "       <a href='#' class='bcross'></a>",
            "   </div>",
            "</div>"
        ].join("\n"));

        $('.uo_adr_list').append(addressHtml);

        $('.uo_adr_list .item').sort(function(a, b) {

            if ($('h3', a).text().toLowerCase() < $('h3', b).text().toLowerCase()) {
                return -1;
            } else {
                return 1;
            }
        }).appendTo('.uo_adr_list');
    }

    function removeAddress(data) {
        $('[data-address-id="' + data.id + '"]').remove();
    }

    // Validation
    function formValidation() {
        let fields = $('form .required');
        let isValid = true;
        fields.removeAttr('style');

        fields.each(function() {
            if( $(this).val().length < 1 ) {
                isValid = false;
                $(this).css({'background': 'rgba(255, 0, 0, .5)'});
            }
        });

        return isValid;
    }

    // Events
    $('form').submit(function (e) {
        e.preventDefault();

        if( formValidation() ) {
            let formData = {
                'name': $('#form_name').val(),
                'city': $('#form_city').val(),
                'area': $('#form_area').val(),
                'street': $('#form_street').val(),
                'house': $('#form_house').val(),
                'add_info': $('#add_info').val()
            };

            sendRequest('/address/create', 'GET', displayAddress, formData,);
        }
    });

    $('.uo_adr_list').on('click', '.bcross', function (e) {
        e.preventDefault();

        let item = $(this).parents('[data-address-id]');
        let itemId = item.attr('data-address-id');

        sendRequest('/address/' + itemId, ajaxType = 'DELETE', removeAddress);
    });
});
